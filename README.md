# Mandelbrot Set Visualizer

A cross-platform application that can visualize the Mandelbrot Set. With some slight modifications, any set similar to the Mandelbrot Set can also be visualized.
The computation shader is limited to double-precision float numbers, so the visualization eventually pixelates as the maximum precision of the data type is reached.
The visualization currently uses an HSV color scheme: black for positions in the set and a range between red and purple for values outside the set depending on how fast they approach infinity.

Controls:

<kbd>R</kbd>: reset the visualization

<kbd>W</kbd>: move up

<kbd>A</kbd>: move left

<kbd>S</kbd>: move down

<kbd>D</kbd>: move down

<kbd>Left Shift</kbd>: zoom in

<kbd>Space</kbd>: zoom out

## Screenshot

![Initial view of the visualizer](images/mandelbrot.png)

## Running

1. Install this project's dependencies: [Zig](https://ziglang.org), [GLFW](https://www.glfw.org/), and [Vulkan](https://www.khronos.org/vulkan/).
   
2. Execute the following in the root directory of the project:
```
zig build run
```
