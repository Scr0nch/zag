const c = @import("../c.zig").c;

pub fn UserFunctions(comptime Uniforms: type) type {
    return struct {
        update: fn (*c.GLFWwindow, uniforms: *Uniforms, time_delta: u64) void,
    };
}
