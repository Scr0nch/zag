const std = @import("std");
const c = @import("../../c.zig").c;
const QueueFamilyIndices = @import("queue_family_indices.zig").QueueFamilyIndices;
const success = @import("success.zig");
const constants = @import("constants.zig");

pub fn create(allocator: std.mem.Allocator, physical_device: c.VkPhysicalDevice, queue_family_indices: QueueFamilyIndices, compute_queue: *c.VkQueue, graphics_queue: *c.VkQueue, present_queue: *c.VkQueue) !c.VkDevice {
    var unique_queue_family_indices: [3]u32 = undefined;
    var queue_create_infos: []c.VkDeviceQueueCreateInfo = undefined;
    if (queue_family_indices.compute_family.? == queue_family_indices.graphics_family.? and queue_family_indices.graphics_family.? == queue_family_indices.present_family.?) {
        unique_queue_family_indices = [3]u32{ queue_family_indices.compute_family.?, 0, 0 };
        queue_create_infos = try allocator.alloc(c.VkDeviceQueueCreateInfo, 1);
    } else {
        unique_queue_family_indices = [3]u32{ queue_family_indices.compute_family.?, queue_family_indices.graphics_family.?, queue_family_indices.present_family.? };
        queue_create_infos = try allocator.alloc(c.VkDeviceQueueCreateInfo, 3);
    }
    defer allocator.free(queue_create_infos);

    const queue_priority: f32 = 1.0;

    for (queue_create_infos, 0..) |_, i| {
        const queue_create_info = c.VkDeviceQueueCreateInfo{
            .sType = c.VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO,
            .queueFamilyIndex = unique_queue_family_indices[i],
            .queueCount = 1,
            .pQueuePriorities = &queue_priority,
            .pNext = null,
            .flags = 0,
        };
        queue_create_infos[i] = queue_create_info;
    }

    const device_features = c.VkPhysicalDeviceFeatures{
        .robustBufferAccess = 0,
        .fullDrawIndexUint32 = 0,
        .imageCubeArray = 0,
        .independentBlend = 0,
        .geometryShader = 0,
        .tessellationShader = 0,
        .sampleRateShading = 0,
        .dualSrcBlend = 0,
        .logicOp = 0,
        .multiDrawIndirect = 0,
        .drawIndirectFirstInstance = 0,
        .depthClamp = 0,
        .depthBiasClamp = 0,
        .fillModeNonSolid = 0,
        .depthBounds = 0,
        .wideLines = 0,
        .largePoints = 0,
        .alphaToOne = 0,
        .multiViewport = 0,
        .samplerAnisotropy = 0,
        .textureCompressionETC2 = 0,
        .textureCompressionASTC_LDR = 0,
        .textureCompressionBC = 0,
        .occlusionQueryPrecise = 0,
        .pipelineStatisticsQuery = 0,
        .vertexPipelineStoresAndAtomics = 0,
        .fragmentStoresAndAtomics = 0,
        .shaderTessellationAndGeometryPointSize = 0,
        .shaderImageGatherExtended = 0,
        .shaderStorageImageExtendedFormats = 0,
        .shaderStorageImageMultisample = 0,
        .shaderStorageImageReadWithoutFormat = 0,
        .shaderStorageImageWriteWithoutFormat = 0,
        .shaderUniformBufferArrayDynamicIndexing = 0,
        .shaderSampledImageArrayDynamicIndexing = 0,
        .shaderStorageBufferArrayDynamicIndexing = 0,
        .shaderStorageImageArrayDynamicIndexing = 0,
        .shaderClipDistance = 0,
        .shaderCullDistance = 0,
        .shaderFloat64 = 1, // TODO check?
        .shaderInt64 = 0,
        .shaderInt16 = 0,
        .shaderResourceResidency = 0,
        .shaderResourceMinLod = 0,
        .sparseBinding = 0,
        .sparseResidencyBuffer = 0,
        .sparseResidencyImage2D = 0,
        .sparseResidencyImage3D = 0,
        .sparseResidency2Samples = 0,
        .sparseResidency4Samples = 0,
        .sparseResidency8Samples = 0,
        .sparseResidency16Samples = 0,
        .sparseResidencyAliased = 0,
        .variableMultisampleRate = 0,
        .inheritedQueries = 0,
    };

    const create_info = c.VkDeviceCreateInfo{
        .sType = c.VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO,

        .queueCreateInfoCount = @intCast(queue_create_infos.len),
        .pQueueCreateInfos = queue_create_infos.ptr,

        .pEnabledFeatures = &device_features,

        .enabledExtensionCount = constants.device_extensions.len,
        .ppEnabledExtensionNames = &constants.device_extensions,
        .enabledLayerCount = if (constants.enable_validation_layers) constants.validation_layers.len else 0,
        .ppEnabledLayerNames = if (constants.enable_validation_layers) &constants.validation_layers else null,

        .pNext = null,
        .flags = 0,
    };

    var logical_device: c.VkDevice = undefined;

    try success.require(c.vkCreateDevice(physical_device, &create_info, null, &logical_device));

    c.vkGetDeviceQueue(logical_device, queue_family_indices.compute_family.?, 0, compute_queue);
    c.vkGetDeviceQueue(logical_device, queue_family_indices.graphics_family.?, 0, graphics_queue);
    c.vkGetDeviceQueue(logical_device, queue_family_indices.present_family.?, 0, present_queue);

    return logical_device;
}

pub fn destroy(logical_device: c.VkDevice) void {
    c.vkDestroyDevice(logical_device, null);
}
