const c = @import("../../c.zig").c;
const std = @import("std");
const success = @import("success.zig");
const Vertex = @import("vertex_constants.zig").Vertex;

pub const Buffer = struct {
    vulkan_buffer: c.VkBuffer,
    memory: c.VkDeviceMemory,
    object_count: u32,
};

fn findMemoryType(physical_device: c.VkPhysicalDevice, type_filter: u32, properties: c.VkMemoryPropertyFlags) !u32 {
    var memory_properties: c.VkPhysicalDeviceMemoryProperties = undefined;
    c.vkGetPhysicalDeviceMemoryProperties(physical_device, &memory_properties);

    var i: u32 = 0;
    while (i < memory_properties.memoryTypeCount) : (i += 1) {
        if ((type_filter & (@as(u32, 1) << @as(u5, @intCast(i))) != 0) and (memory_properties.memoryTypes[i].propertyFlags & properties) == properties) {
            return i;
        }
    }

    return error.NoSuitableMemoryType;
}

fn copyDataToBuffer(logical_device: c.VkDevice, comptime T: type, data_array: []const T, memory: c.VkDeviceMemory, buffer_size: u64) !void {
    var buffer_data_ptr: ?*anyopaque = undefined;
    try success.require(c.vkMapMemory(logical_device, memory, 0, buffer_size, 0, &buffer_data_ptr));
    for (data_array, 0..) |data, i| {
        @as([*]T, @alignCast(@ptrCast(buffer_data_ptr)))[i] = data;
    }
    c.vkUnmapMemory(logical_device, memory);
}

fn createBuffer(physical_device: c.VkPhysicalDevice, logical_device: c.VkDevice, comptime T: type, data_array: []const T, usage: c.VkBufferUsageFlags, properties: c.VkMemoryPropertyFlags) !Buffer {
    const buffer_info: c.VkBufferCreateInfo = c.VkBufferCreateInfo{
        .sType = c.VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO,
        .size = @sizeOf(T) * data_array.len,
        .usage = usage,
        .sharingMode = c.VK_SHARING_MODE_EXCLUSIVE,
        .queueFamilyIndexCount = 0,
        .pQueueFamilyIndices = null,
        .flags = 0,
        .pNext = null,
    };

    var buffer: c.VkBuffer = undefined;

    try success.require(c.vkCreateBuffer(logical_device, &buffer_info, null, &buffer));
    errdefer c.vkDestroyBuffer(logical_device, buffer, null);

    var memory_requirements: c.VkMemoryRequirements = undefined;
    c.vkGetBufferMemoryRequirements(logical_device, buffer, &memory_requirements);

    const allocation_info = c.VkMemoryAllocateInfo{
        .sType = c.VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO,
        .allocationSize = memory_requirements.size,
        .memoryTypeIndex = try findMemoryType(physical_device, memory_requirements.memoryTypeBits, properties),
        .pNext = null,
    };

    var memory: c.VkDeviceMemory = undefined;

    try success.require(c.vkAllocateMemory(logical_device, &allocation_info, null, &memory));
    errdefer c.vkFreeMemory(logical_device, memory, null);

    try success.require(c.vkBindBufferMemory(logical_device, buffer, memory, 0));

    try copyDataToBuffer(logical_device, T, data_array, memory, buffer_info.size);

    return Buffer{
        .vulkan_buffer = buffer,
        .memory = memory,
        .object_count = @intCast(data_array.len),
    };
}

pub fn createVertexBuffer(physical_device: c.VkPhysicalDevice, logical_device: c.VkDevice) !Buffer {
    const size: f32 = 10;
    const vertices: []const Vertex = ([_]Vertex{
        Vertex{ .position = [2]f32{ -size, -size }, .color = [3]f32{ 1.0, 0.0, 0.0 } },
        Vertex{ .position = [2]f32{ size, -size }, .color = [3]f32{ 0.0, 1.0, 0.0 } },
        Vertex{ .position = [2]f32{ size, size }, .color = [3]f32{ 0.0, 0.0, 1.0 } },
        Vertex{ .position = [2]f32{ -size, size }, .color = [3]f32{ 1.0, 1.0, 1.0 } },
    })[0..];

    return createBuffer(physical_device, logical_device, Vertex, vertices, c.VK_BUFFER_USAGE_VERTEX_BUFFER_BIT, c.VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | c.VK_MEMORY_PROPERTY_HOST_COHERENT_BIT);
}

pub fn createIndexBuffer(physical_device: c.VkPhysicalDevice, logical_device: c.VkDevice) !Buffer {
    const indices: [6]u16 = [_]u16{
        0, 1, 2, 2, 3, 0,
    };

    return createBuffer(physical_device, logical_device, u16, &indices, c.VK_BUFFER_USAGE_INDEX_BUFFER_BIT, c.VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | c.VK_MEMORY_PROPERTY_HOST_COHERENT_BIT);
}

/// caller must free uniform buffers
pub fn createUniformBuffers(comptime Uniforms: type, allocator: std.mem.Allocator, physical_device: c.VkPhysicalDevice, logical_device: c.VkDevice, swapchain_image_count: usize) ![]Buffer {
    const uniforms: Uniforms = undefined;

    const uniforms_array: [1]Uniforms = [_]Uniforms{
        uniforms,
    };

    var buffers: []Buffer = try allocator.alloc(Buffer, swapchain_image_count);
    var i: usize = 0;
    while (i < swapchain_image_count) : (i += 1) {
        buffers[i] = try createBuffer(physical_device, logical_device, Uniforms, &uniforms_array, c.VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT, c.VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | c.VK_MEMORY_PROPERTY_HOST_COHERENT_BIT);
    }

    return buffers;
}

pub fn updateUniformBuffer(comptime Uniforms: type, logical_device: c.VkDevice, buffer: Buffer, uniforms: *Uniforms) !void {
    const uniforms_array: [1]Uniforms = [_]Uniforms{
        uniforms.*,
    };
    try copyDataToBuffer(logical_device, Uniforms, &uniforms_array, buffer.memory, @sizeOf(Uniforms));
}

pub fn destroyBuffer(logical_device: c.VkDevice, buffer: Buffer) void {
    c.vkDestroyBuffer(logical_device, buffer.vulkan_buffer, null);
    c.vkFreeMemory(logical_device, buffer.memory, null);
}
