pub const max_frames_in_flight = 2;
pub const enable_validation_layers = @import("std").debug.runtime_safety;
pub const validation_layers = [_][*:0]const u8{"VK_LAYER_KHRONOS_validation"};
pub const device_extensions = [_][*:0]const u8{@import("../../c.zig").c.VK_KHR_SWAPCHAIN_EXTENSION_NAME};
