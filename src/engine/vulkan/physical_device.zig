const std = @import("std");
const c = @import("../../c.zig").c;
const success = @import("success.zig");
const queue_family_indices = @import("queue_family_indices.zig");
const QueueFamilyIndices = queue_family_indices.QueueFamilyIndices;
const swap_chain_support = @import("swap_chain_support.zig");
const SwapchainSupportDetails = swap_chain_support.SwapchainSupportDetails;
const device_extensions = @import("constants.zig").device_extensions;

/// caller must destroy SwapChainSupport
pub fn pick(allocator: std.mem.Allocator, instance: c.VkInstance, surface: c.VkSurfaceKHR, pIndices: *QueueFamilyIndices, pSwapChainSupport: *SwapchainSupportDetails) !c.VkPhysicalDevice {
    var device_count: u32 = 0;
    try success.require(c.vkEnumeratePhysicalDevices(instance, &device_count, null));

    if (device_count == 0) {
        return error.FailedToFindGPUsWithVulkanSupport;
    }

    const devices = try allocator.alloc(c.VkPhysicalDevice, device_count);
    defer allocator.free(devices);
    try success.require(c.vkEnumeratePhysicalDevices(instance, &device_count, devices.ptr));

    return for (devices) |device| {
        if (try isDeviceSuitable(allocator, surface, device, pIndices, pSwapChainSupport)) {
            break device;
        }
    } else error.FailedToFindSuitableGPU;
}

/// caller must destroy SwapChainSupport
fn isDeviceSuitable(allocator: std.mem.Allocator, surface: c.VkSurfaceKHR, physical_device: c.VkPhysicalDevice, pIndices: *QueueFamilyIndices, pSwapChainSupport: *SwapchainSupportDetails) !bool {
    pIndices.* = try queue_family_indices.get(allocator, surface, physical_device);
    if (!pIndices.*.isComplete()) {
        return false;
    }

    const is_extensions_supported = try isDeviceExtensionsSupported(allocator, physical_device);
    if (!is_extensions_supported) {
        return false;
    }

    pSwapChainSupport.* = try swap_chain_support.create(allocator, surface, physical_device);
    return pSwapChainSupport.*.formats.items.len != 0 and pSwapChainSupport.*.present_modes.items.len != 0;
}

fn isDeviceExtensionsSupported(allocator: std.mem.Allocator, device: c.VkPhysicalDevice) !bool {
    var extension_count: u32 = undefined;
    try success.require(c.vkEnumerateDeviceExtensionProperties(device, null, &extension_count, null));

    const available_extensions = try allocator.alloc(c.VkExtensionProperties, extension_count);
    defer allocator.free(available_extensions);
    try success.require(c.vkEnumerateDeviceExtensionProperties(device, null, &extension_count, available_extensions.ptr));

    // the example code at vulkan-tutorial.com uses a vector to store all of the required extensions, however,
    // the use of a hash map is probably an optimization worth keeping
    var required_extensions = std.ArrayList([*:0]const u8).init(allocator);
    defer required_extensions.deinit();
    for (device_extensions) |device_extension| {
        _ = try required_extensions.append(device_extension);
    }

    for (available_extensions) |extension| {
        for (device_extensions, 0..) |device_extension, i| {
            if (std.mem.orderZ(u8, @as([*:0]const u8, @ptrCast(&extension.extensionName)), device_extension) == .eq) {
                _ = required_extensions.swapRemove(i);
            }
        }
    }

    return required_extensions.items.len == 0;
}
