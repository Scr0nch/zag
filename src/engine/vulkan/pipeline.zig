const c = @import("../../c.zig").c;
const std = @import("std");
const mem = std.mem;
const success = @import("success.zig");
const vertex_constants = @import("vertex_constants.zig");

pub fn createRenderPass(logical_device: c.VkDevice, swapchain_image_format: c.VkFormat) !c.VkRenderPass {
    const color_attachment = c.VkAttachmentDescription{
        .format = swapchain_image_format,
        .samples = c.VK_SAMPLE_COUNT_1_BIT,
        .loadOp = c.VK_ATTACHMENT_LOAD_OP_LOAD,
        .storeOp = c.VK_ATTACHMENT_STORE_OP_DONT_CARE,
        .stencilLoadOp = c.VK_ATTACHMENT_LOAD_OP_DONT_CARE,
        .stencilStoreOp = c.VK_ATTACHMENT_STORE_OP_DONT_CARE,
        .initialLayout = c.VK_IMAGE_LAYOUT_GENERAL,
        .finalLayout = c.VK_IMAGE_LAYOUT_PRESENT_SRC_KHR,
        .flags = 0,
    };

    const color_attachment_reference = c.VkAttachmentReference{
        .attachment = 0,
        .layout = c.VK_IMAGE_LAYOUT_GENERAL, //c.VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL),
    };

    const subpass = [_]c.VkSubpassDescription{c.VkSubpassDescription{
        .pipelineBindPoint = c.VK_PIPELINE_BIND_POINT_GRAPHICS,
        .colorAttachmentCount = 1,
        .pColorAttachments = @as(*const [1]c.VkAttachmentReference, &color_attachment_reference),

        .flags = 0,
        .inputAttachmentCount = 0,
        .pInputAttachments = null,
        .pResolveAttachments = null,
        .pDepthStencilAttachment = null,
        .preserveAttachmentCount = 0,
        .pPreserveAttachments = null,
    }};

    const dependency = [_]c.VkSubpassDependency{.{
        .srcSubpass = c.VK_SUBPASS_EXTERNAL,
        .dstSubpass = 0,
        .srcStageMask = c.VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT,
        .srcAccessMask = 0,
        .dstStageMask = c.VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT,
        .dstAccessMask = c.VK_ACCESS_COLOR_ATTACHMENT_READ_BIT | c.VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT,

        .dependencyFlags = 0,
    }};

    const render_pass_info = c.VkRenderPassCreateInfo{
        .sType = c.VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO,
        .attachmentCount = 1,
        .pAttachments = @as(*const [1]c.VkAttachmentDescription, &color_attachment),
        .subpassCount = 1,
        .pSubpasses = &subpass,
        .dependencyCount = 1,
        .pDependencies = &dependency,

        .pNext = null,
        .flags = 0,
    };

    var render_pass: c.VkRenderPass = undefined;

    try success.require(c.vkCreateRenderPass(logical_device, &render_pass_info, null, &render_pass));

    return render_pass;
}

fn createShaderModule(logical_device: c.VkDevice, code: []align(@alignOf(u32)) const u8) !c.VkShaderModule {
    const shader_info = c.VkShaderModuleCreateInfo{
        .sType = c.VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO,
        .codeSize = code.len,
        .pCode = mem.bytesAsSlice(u32, code).ptr,

        .pNext = null,
        .flags = 0,
    };

    var shader_module: c.VkShaderModule = undefined;

    try success.require(c.vkCreateShaderModule(logical_device, &shader_info, null, &shader_module));

    return shader_module;
}

pub fn createGraphicsPipeline(logical_device: c.VkDevice, swapchain_extent: c.VkExtent2D, descriptor_set_layout: c.VkDescriptorSetLayout, pipeline_layout: *c.VkPipelineLayout, render_pass: c.VkRenderPass) !c.VkPipeline {
    // create pipeline layout
    const pipeline_layout_info = c.VkPipelineLayoutCreateInfo{
        .sType = c.VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO,
        .setLayoutCount = 1,
        .pSetLayouts = &descriptor_set_layout,
        .pushConstantRangeCount = 0,
        .pNext = null,
        .flags = 0,
        .pPushConstantRanges = null,
    };

    try success.require(c.vkCreatePipelineLayout(logical_device, &pipeline_layout_info, null, pipeline_layout));

    // create the actual pipeline
    const vertex_shader_code: *const @alignOf(u32)[u8] = @alignCast(@embedFile("../../../zig-cache/shaders/vert.spv"));
    const fragment_shader_code: *const @alignOf(u32)[u8] = @alignCast(@embedFile("../../../zig-cache/shaders/frag.spv"));

    const vertex_shader_module = try createShaderModule(logical_device, vertex_shader_code);
    const fragment_shader_module = try createShaderModule(logical_device, fragment_shader_code);

    const vertex_shader_stage_info = c.VkPipelineShaderStageCreateInfo{
        .sType = c.VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO,
        .stage = c.VK_SHADER_STAGE_VERTEX_BIT,
        .module = vertex_shader_module,
        .pName = "main",

        .pNext = null,
        .flags = 0,
        .pSpecializationInfo = null,
    };

    const fragment_shader_stage_info = c.VkPipelineShaderStageCreateInfo{
        .sType = c.VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO,
        .stage = c.VK_SHADER_STAGE_FRAGMENT_BIT,
        .module = fragment_shader_module,
        .pName = "main",
        .pNext = null,
        .flags = 0,

        .pSpecializationInfo = null,
    };

    const shader_stages = [_]c.VkPipelineShaderStageCreateInfo{ vertex_shader_stage_info, fragment_shader_stage_info };

    const vertex_input_info = c.VkPipelineVertexInputStateCreateInfo{
        .sType = c.VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO,
        .vertexBindingDescriptionCount = 1,
        .pVertexBindingDescriptions = &vertex_constants.vertex_binding_description,
        .vertexAttributeDescriptionCount = 2,
        .pVertexAttributeDescriptions = &vertex_constants.vertex_attribute_descriptions,
        .pNext = null,
        .flags = 0,
    };

    const input_assembly_info = c.VkPipelineInputAssemblyStateCreateInfo{
        .sType = c.VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO,
        .topology = c.VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST,
        .primitiveRestartEnable = c.VK_FALSE,
        .pNext = null,
        .flags = 0,
    };

    const viewport = [_]c.VkViewport{c.VkViewport{
        .x = 0.0,
        .y = 0.0,
        .width = @enumFromInt(swapchain_extent.width),
        .height = @enumFromInt(swapchain_extent.height),
        .minDepth = 0.0,
        .maxDepth = 1.0,
    }};

    const scissor = [_]c.VkRect2D{c.VkRect2D{
        .offset = c.VkOffset2D{ .x = 0, .y = 0 },
        .extent = swapchain_extent,
    }};

    const viewport_state_info = c.VkPipelineViewportStateCreateInfo{
        .sType = c.VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO,
        .viewportCount = 1,
        .pViewports = &viewport,
        .scissorCount = 1,
        .pScissors = &scissor,

        .pNext = null,
        .flags = 0,
    };

    const rasterizer_info = c.VkPipelineRasterizationStateCreateInfo{
        .sType = c.VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO,
        .depthClampEnable = c.VK_FALSE,
        .rasterizerDiscardEnable = c.VK_FALSE,
        .polygonMode = c.VK_POLYGON_MODE_FILL,
        .lineWidth = 1.0,
        .cullMode = c.VK_CULL_MODE_BACK_BIT,
        .frontFace = c.VK_FRONT_FACE_CLOCKWISE,
        .depthBiasEnable = c.VK_FALSE,

        .pNext = null,
        .flags = 0,
        .depthBiasConstantFactor = 0,
        .depthBiasClamp = 0,
        .depthBiasSlopeFactor = 0,
    };

    const multisampling_info = c.VkPipelineMultisampleStateCreateInfo{
        .sType = c.VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO,
        .sampleShadingEnable = c.VK_FALSE,
        .rasterizationSamples = c.VK_SAMPLE_COUNT_1_BIT,
        .pNext = null,
        .flags = 0,
        .minSampleShading = 0,
        .pSampleMask = null,
        .alphaToCoverageEnable = 0,
        .alphaToOneEnable = 0,
    };

    const color_blend_attachment = c.VkPipelineColorBlendAttachmentState{
        .colorWriteMask = c.VK_COLOR_COMPONENT_R_BIT | c.VK_COLOR_COMPONENT_G_BIT | c.VK_COLOR_COMPONENT_B_BIT | c.VK_COLOR_COMPONENT_A_BIT,
        .blendEnable = c.VK_FALSE,

        .srcColorBlendFactor = c.VK_BLEND_FACTOR_ZERO,
        .dstColorBlendFactor = c.VK_BLEND_FACTOR_ZERO,
        .colorBlendOp = c.VK_BLEND_OP_ADD,
        .srcAlphaBlendFactor = c.VK_BLEND_FACTOR_ZERO,
        .dstAlphaBlendFactor = c.VK_BLEND_FACTOR_ZERO,
        .alphaBlendOp = c.VK_BLEND_OP_ADD,
    };

    const color_blending_info = c.VkPipelineColorBlendStateCreateInfo{
        .sType = c.VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO,
        .logicOpEnable = c.VK_FALSE,
        .logicOp = c.VK_LOGIC_OP_COPY,
        .attachmentCount = 1,
        .pAttachments = &color_blend_attachment,
        .blendConstants = [_]f32{ 0, 0, 0, 0 },

        .pNext = null,
        .flags = 0,
    };

    const pipeline_info = [_]c.VkGraphicsPipelineCreateInfo{c.VkGraphicsPipelineCreateInfo{
        .sType = c.VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO,
        .stageCount = @intCast(shader_stages.len),
        .pStages = &shader_stages,
        .pVertexInputState = &vertex_input_info,
        .pInputAssemblyState = &input_assembly_info,
        .pViewportState = &viewport_state_info,
        .pRasterizationState = &rasterizer_info,
        .pMultisampleState = &multisampling_info,
        .pColorBlendState = &color_blending_info,
        .layout = pipeline_layout.*,
        .renderPass = render_pass,
        .subpass = 0,
        .basePipelineHandle = null,

        .pNext = null,
        .flags = 0,
        .pTessellationState = null,
        .pDepthStencilState = null,
        .pDynamicState = null,
        .basePipelineIndex = 0,
    }};

    var pipeline: c.VkPipeline = undefined;

    try success.require(c.vkCreateGraphicsPipelines(
        logical_device,
        null,
        @intCast(pipeline_info.len),
        &pipeline_info,
        null,
        @as(*[1]c.VkPipeline, &pipeline),
    ));

    c.vkDestroyShaderModule(logical_device, fragment_shader_module, null);
    c.vkDestroyShaderModule(logical_device, vertex_shader_module, null);

    return pipeline;
}

pub fn createComputePipeline(logical_device: c.VkDevice, descriptor_set_layout: c.VkDescriptorSetLayout, pipeline_layout: *c.VkPipelineLayout) !c.VkPipeline {
    const compute_shader_code: []align(@alignOf(u32)) const u8 = @ptrCast(@alignCast(@embedFile("../../shaders/compute.spv")));

    const compute_shader_module = try createShaderModule(logical_device, compute_shader_code);

    const compute_shader_stage_info = .{
        .sType = c.VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO,
        .stage = c.VK_SHADER_STAGE_COMPUTE_BIT,
        .module = compute_shader_module,
        .pName = "main",

        .pNext = null,
        .flags = 0,
        .pSpecializationInfo = null,
    };

    const pipeline_layout_create_info: c.VkPipelineLayoutCreateInfo = .{
        .sType = c.VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO,
        .setLayoutCount = 1,
        .pSetLayouts = &descriptor_set_layout,
        .pushConstantRangeCount = 0,
        .pPushConstantRanges = null,
        .flags = 0,
        .pNext = null,
    };

    try success.require(c.vkCreatePipelineLayout(logical_device, &pipeline_layout_create_info, null, pipeline_layout));

    const pipeline_create_info: c.VkComputePipelineCreateInfo = .{
        .sType = c.VK_STRUCTURE_TYPE_COMPUTE_PIPELINE_CREATE_INFO,
        .stage = compute_shader_stage_info,
        .layout = pipeline_layout.*,
        .basePipelineHandle = null,
        .basePipelineIndex = 0,
        .flags = 0,
        .pNext = null,
    };

    var pipeline: c.VkPipeline = undefined;

    try success.require(c.vkCreateComputePipelines(logical_device, null, 1, &pipeline_create_info, null, &pipeline));

    c.vkDestroyShaderModule(logical_device, compute_shader_module, null);

    return pipeline;
}

pub fn destroyRenderPass(logical_device: c.VkDevice, render_pass: c.VkRenderPass) void {
    c.vkDestroyRenderPass(logical_device, render_pass, null);
}

pub fn destroy(logical_device: c.VkDevice, pipeline_layout: c.VkPipelineLayout, pipeline: c.VkPipeline) void {
    c.vkDestroyPipelineLayout(logical_device, pipeline_layout, null);
    c.vkDestroyPipeline(logical_device, pipeline, null);
}
