#version 450
#extension GL_ARB_separate_shader_objects : enable

layout (location = 0) in vec2 input_texture_coordinate;

layout (location = 0) out vec4 output_color;

layout (binding = 0) uniform sampler2D image_sampler;

void main() {
    output_color = texture(image_sampler, input_texture_coordinate);
}