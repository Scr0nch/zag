pub const Uniforms = packed struct {
    width: f32 = 0,
    height: f32 = 0,
    translation_x: f64,
    translation_y: f64,
    scale: f64,
};
